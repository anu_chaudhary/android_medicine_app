package com.medicineapp.roomdb;

import java.lang.System;

@androidx.room.Dao()
@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\b\u0003\bg\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\'J\u0010\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\bH\'J\u0014\u0010\t\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u000b0\nH\'J\u0010\u0010\f\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\'J\u0010\u0010\r\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\'\u00a8\u0006\u000e"}, d2 = {"Lcom/medicineapp/roomdb/MedicineDao;", "", "deleteMedicine", "", "medicine", "Lcom/medicineapp/roomdb/Medicine;", "getMedicineInfo", "medicineId", "", "getMedicineList", "Landroidx/lifecycle/LiveData;", "", "insertMedicine", "updateMedicine", "app_debug"})
public abstract interface MedicineDao {
    
    @androidx.room.Insert()
    public abstract void insertMedicine(@org.jetbrains.annotations.NotNull()
    com.medicineapp.roomdb.Medicine medicine);
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * FROM medicine_table")
    public abstract androidx.lifecycle.LiveData<java.util.List<com.medicineapp.roomdb.Medicine>> getMedicineList();
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.Query(value = "SELECT * FROM medicine_table WHERE id == :medicineId")
    public abstract com.medicineapp.roomdb.Medicine getMedicineInfo(int medicineId);
    
    @androidx.room.Update()
    public abstract void updateMedicine(@org.jetbrains.annotations.NotNull()
    com.medicineapp.roomdb.Medicine medicine);
    
    @androidx.room.Delete()
    public abstract void deleteMedicine(@org.jetbrains.annotations.NotNull()
    com.medicineapp.roomdb.Medicine medicine);
}
package com.medicineapp.ui.addmedicine;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\'\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u000e\u0010\u0006\u001a\n\u0018\u00010\u0007j\u0004\u0018\u0001`\b\u00a2\u0006\u0002\u0010\tJ\t\u0010\u0010\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u0011\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u0011\u0010\u0012\u001a\n\u0018\u00010\u0007j\u0004\u0018\u0001`\bH\u00c6\u0003J1\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0010\b\u0002\u0010\u0006\u001a\n\u0018\u00010\u0007j\u0004\u0018\u0001`\bH\u00c6\u0001J\u0013\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001R\u0019\u0010\u0006\u001a\n\u0018\u00010\u0007j\u0004\u0018\u0001`\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000f\u00a8\u0006\u001b"}, d2 = {"Lcom/medicineapp/ui/addmedicine/AddMedicineResponse;", "", "status", "Lcom/medicineapp/ui/addmedicine/Status;", "medicine", "Lcom/medicineapp/roomdb/Medicine;", "error", "Ljava/lang/Exception;", "Lkotlin/Exception;", "(Lcom/medicineapp/ui/addmedicine/Status;Lcom/medicineapp/roomdb/Medicine;Ljava/lang/Exception;)V", "getError", "()Ljava/lang/Exception;", "getMedicine", "()Lcom/medicineapp/roomdb/Medicine;", "getStatus", "()Lcom/medicineapp/ui/addmedicine/Status;", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "", "app_debug"})
public final class AddMedicineResponse {
    @org.jetbrains.annotations.NotNull()
    private final com.medicineapp.ui.addmedicine.Status status = null;
    @org.jetbrains.annotations.Nullable()
    private final com.medicineapp.roomdb.Medicine medicine = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.Exception error = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.medicineapp.ui.addmedicine.Status getStatus() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.medicineapp.roomdb.Medicine getMedicine() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Exception getError() {
        return null;
    }
    
    public AddMedicineResponse(@org.jetbrains.annotations.NotNull()
    com.medicineapp.ui.addmedicine.Status status, @org.jetbrains.annotations.Nullable()
    com.medicineapp.roomdb.Medicine medicine, @org.jetbrains.annotations.Nullable()
    java.lang.Exception error) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.medicineapp.ui.addmedicine.Status component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.medicineapp.roomdb.Medicine component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Exception component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.medicineapp.ui.addmedicine.AddMedicineResponse copy(@org.jetbrains.annotations.NotNull()
    com.medicineapp.ui.addmedicine.Status status, @org.jetbrains.annotations.Nullable()
    com.medicineapp.roomdb.Medicine medicine, @org.jetbrains.annotations.Nullable()
    java.lang.Exception error) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object p0) {
        return false;
    }
}
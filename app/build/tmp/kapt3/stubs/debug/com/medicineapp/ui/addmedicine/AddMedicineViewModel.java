package com.medicineapp.ui.addmedicine;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J&\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\rJ\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006J\u000e\u0010\u0012\u001a\u00020\u000b2\u0006\u0010\u0013\u001a\u00020\u0014J\u000e\u0010\u0015\u001a\u00020\u000b2\u0006\u0010\u0016\u001a\u00020\u0017J\u001e\u0010\u0018\u001a\u00020\u000b2\u0006\u0010\u0019\u001a\u00020\r2\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001bJ&\u0010\u001d\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\rR\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"}, d2 = {"Lcom/medicineapp/ui/addmedicine/AddMedicineViewModel;", "Landroidx/lifecycle/AndroidViewModel;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "mutableAddMedicineData", "Landroidx/lifecycle/MutableLiveData;", "Lcom/medicineapp/ui/addmedicine/AddMedicineResponse;", "repository", "Lcom/medicineapp/ui/addmedicine/MedicineRepository;", "addMedicine", "", "name", "", "desc", "price", "qty", "getLiveData", "getMedicineInfo", "medicineId", "", "updateMedicine", "medicine", "Lcom/medicineapp/roomdb/Medicine;", "validateFields", "value", "success", "Lcom/medicineapp/ui/addmedicine/Status;", "error", "validateForm", "app_debug"})
public final class AddMedicineViewModel extends androidx.lifecycle.AndroidViewModel {
    private com.medicineapp.ui.addmedicine.MedicineRepository repository;
    private androidx.lifecycle.MutableLiveData<com.medicineapp.ui.addmedicine.AddMedicineResponse> mutableAddMedicineData;
    
    /**
     * return liva data to the view to be observe applied changes
     */
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.medicineapp.ui.addmedicine.AddMedicineResponse> getLiveData() {
        return null;
    }
    
    /**
     * fetch medicine details from data base in case of any CURD operations
     */
    public final void getMedicineInfo(int medicineId) {
    }
    
    public final void validateForm(@org.jetbrains.annotations.NotNull()
    java.lang.String name, @org.jetbrains.annotations.NotNull()
    java.lang.String desc, @org.jetbrains.annotations.NotNull()
    java.lang.String price, @org.jetbrains.annotations.NotNull()
    java.lang.String qty) {
    }
    
    public final void addMedicine(@org.jetbrains.annotations.NotNull()
    java.lang.String name, @org.jetbrains.annotations.NotNull()
    java.lang.String desc, @org.jetbrains.annotations.NotNull()
    java.lang.String price, @org.jetbrains.annotations.NotNull()
    java.lang.String qty) {
    }
    
    public final void updateMedicine(@org.jetbrains.annotations.NotNull()
    com.medicineapp.roomdb.Medicine medicine) {
    }
    
    public final void validateFields(@org.jetbrains.annotations.NotNull()
    java.lang.String value, @org.jetbrains.annotations.NotNull()
    com.medicineapp.ui.addmedicine.Status success, @org.jetbrains.annotations.NotNull()
    com.medicineapp.ui.addmedicine.Status error) {
    }
    
    public AddMedicineViewModel(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super(null);
    }
}
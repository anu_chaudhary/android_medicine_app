package com.medicineapp.ui.addmedicine;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001:\u0001\u000fB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u000e\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\bJ\u000e\u0010\u000e\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\bR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"}, d2 = {"Lcom/medicineapp/ui/addmedicine/MedicineRepository;", "", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "medicineDao", "Lcom/medicineapp/roomdb/MedicineDao;", "getMedicine", "Lcom/medicineapp/roomdb/Medicine;", "medicineId", "", "insertMedicine", "", "medicine", "updateMedicine", "UpdateMedicineAsyncTask", "app_debug"})
public final class MedicineRepository {
    private com.medicineapp.roomdb.MedicineDao medicineDao;
    
    public final void insertMedicine(@org.jetbrains.annotations.NotNull()
    com.medicineapp.roomdb.Medicine medicine) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.medicineapp.roomdb.Medicine getMedicine(int medicineId) {
        return null;
    }
    
    public final void updateMedicine(@org.jetbrains.annotations.NotNull()
    com.medicineapp.roomdb.Medicine medicine) {
    }
    
    public MedicineRepository(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0011\n\u0002\b\u0002\b\u0002\u0018\u00002\u0014\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00030\u0001B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J%\u0010\t\u001a\u00020\u00032\u0016\u0010\n\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u00020\u000b\"\u0004\u0018\u00010\u0002H\u0014\u00a2\u0006\u0002\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\b\u00a8\u0006\r"}, d2 = {"Lcom/medicineapp/ui/addmedicine/MedicineRepository$UpdateMedicineAsyncTask;", "Landroid/os/AsyncTask;", "Lcom/medicineapp/roomdb/Medicine;", "", "medicineDao", "Lcom/medicineapp/roomdb/MedicineDao;", "(Lcom/medicineapp/roomdb/MedicineDao;)V", "getMedicineDao", "()Lcom/medicineapp/roomdb/MedicineDao;", "doInBackground", "p0", "", "([Lcom/medicineapp/roomdb/Medicine;)V", "app_debug"})
    static final class UpdateMedicineAsyncTask extends android.os.AsyncTask<com.medicineapp.roomdb.Medicine, kotlin.Unit, kotlin.Unit> {
        @org.jetbrains.annotations.NotNull()
        private final com.medicineapp.roomdb.MedicineDao medicineDao = null;
        
        @org.jetbrains.annotations.NotNull()
        public final com.medicineapp.roomdb.MedicineDao getMedicineDao() {
            return null;
        }
        
        @java.lang.Override()
        protected void doInBackground(@org.jetbrains.annotations.NotNull()
        com.medicineapp.roomdb.Medicine... p0) {
        }
        
        public UpdateMedicineAsyncTask(@org.jetbrains.annotations.NotNull()
        com.medicineapp.roomdb.MedicineDao medicineDao) {
            super();
        }
    }
}
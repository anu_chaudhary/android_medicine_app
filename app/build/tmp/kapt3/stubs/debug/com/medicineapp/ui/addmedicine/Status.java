package com.medicineapp.ui.addmedicine;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\f\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\f\u00a8\u0006\r"}, d2 = {"Lcom/medicineapp/ui/addmedicine/Status;", "", "(Ljava/lang/String;I)V", "NAME_VALIDATION_ERROR", "DESC_VALIDATION_ERROR", "PRICE_VALIDATION_ERROR", "QTY_VALIDATION_ERROR", "NAME_VALIDATION_SUCCESS", "DESC_VALIDATION_SUCCESS", "PRICE_VALIDATION_SUCCESS", "QTY_VALIDATION_SUCCESS", "SUCCESS", "GET_MEDICINE_INFO_SUCCESS", "app_debug"})
public enum Status {
    /*public static final*/ NAME_VALIDATION_ERROR /* = new NAME_VALIDATION_ERROR() */,
    /*public static final*/ DESC_VALIDATION_ERROR /* = new DESC_VALIDATION_ERROR() */,
    /*public static final*/ PRICE_VALIDATION_ERROR /* = new PRICE_VALIDATION_ERROR() */,
    /*public static final*/ QTY_VALIDATION_ERROR /* = new QTY_VALIDATION_ERROR() */,
    /*public static final*/ NAME_VALIDATION_SUCCESS /* = new NAME_VALIDATION_SUCCESS() */,
    /*public static final*/ DESC_VALIDATION_SUCCESS /* = new DESC_VALIDATION_SUCCESS() */,
    /*public static final*/ PRICE_VALIDATION_SUCCESS /* = new PRICE_VALIDATION_SUCCESS() */,
    /*public static final*/ QTY_VALIDATION_SUCCESS /* = new QTY_VALIDATION_SUCCESS() */,
    /*public static final*/ SUCCESS /* = new SUCCESS() */,
    /*public static final*/ GET_MEDICINE_INFO_SUCCESS /* = new GET_MEDICINE_INFO_SUCCESS() */;
    
    Status() {
    }
}
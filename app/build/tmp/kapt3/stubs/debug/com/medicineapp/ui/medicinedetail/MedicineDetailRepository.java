package com.medicineapp.ui.medicinedetail;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"Lcom/medicineapp/ui/medicinedetail/MedicineDetailRepository;", "", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "medicineDao", "Lcom/medicineapp/roomdb/MedicineDao;", "getMedicineDetail", "Lcom/medicineapp/roomdb/Medicine;", "medicineId", "", "app_debug"})
public final class MedicineDetailRepository {
    private com.medicineapp.roomdb.MedicineDao medicineDao;
    
    @org.jetbrains.annotations.NotNull()
    public final com.medicineapp.roomdb.Medicine getMedicineDetail(int medicineId) {
        return null;
    }
    
    public MedicineDetailRepository(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super();
    }
}
package com.medicineapp.ui.medicinedetail;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006J\u000e\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eR\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"}, d2 = {"Lcom/medicineapp/ui/medicinedetail/MedicineDetailViewModel;", "Landroidx/lifecycle/AndroidViewModel;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "medicineDetailData", "Landroidx/lifecycle/MutableLiveData;", "Lcom/medicineapp/ui/addmedicine/AddMedicineResponse;", "repository", "Lcom/medicineapp/ui/medicinedetail/MedicineDetailRepository;", "getLiveData", "getMedicineInfo", "", "medicineId", "", "app_debug"})
public final class MedicineDetailViewModel extends androidx.lifecycle.AndroidViewModel {
    private com.medicineapp.ui.medicinedetail.MedicineDetailRepository repository;
    private androidx.lifecycle.MutableLiveData<com.medicineapp.ui.addmedicine.AddMedicineResponse> medicineDetailData;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.medicineapp.ui.addmedicine.AddMedicineResponse> getLiveData() {
        return null;
    }
    
    public final void getMedicineInfo(int medicineId) {
    }
    
    public MedicineDetailViewModel(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super(null);
    }
}
package com.medicineapp.ui.medicinelist;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\u0015B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\t\u001a\u00020\nH\u0014J\u0012\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0014J\u0010\u0010\u000f\u001a\u00020\f2\u0006\u0010\u0010\u001a\u00020\nH\u0002J\u000e\u0010\u0011\u001a\u00020\f2\u0006\u0010\u0010\u001a\u00020\nJ\u000e\u0010\u0012\u001a\u00020\f2\u0006\u0010\u0013\u001a\u00020\u0014R\u001b\u0010\u0003\u001a\u00020\u00048BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0016"}, d2 = {"Lcom/medicineapp/ui/medicinelist/MedicineActivity;", "Lcom/medicineapp/base/BaseActivity;", "()V", "medicineViewModel", "Lcom/medicineapp/ui/medicinelist/MedicineViewModel;", "getMedicineViewModel", "()Lcom/medicineapp/ui/medicinelist/MedicineViewModel;", "medicineViewModel$delegate", "Lkotlin/Lazy;", "getResourceId", "", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "openAddMedicineDialog", "medicineId", "openMedicineDetailPage", "showAlertDialog", "medicine", "Lcom/medicineapp/roomdb/Medicine;", "OnItemClickListener", "app_debug"})
public final class MedicineActivity extends com.medicineapp.base.BaseActivity {
    private final kotlin.Lazy medicineViewModel$delegate = null;
    private java.util.HashMap _$_findViewCache;
    
    private final com.medicineapp.ui.medicinelist.MedicineViewModel getMedicineViewModel() {
        return null;
    }
    
    @java.lang.Override()
    protected int getResourceId() {
        return 0;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    /**
     * open detail page as per medicine selection
     */
    public final void openMedicineDetailPage(int medicineId) {
    }
    
    private final void openAddMedicineDialog(int medicineId) {
    }
    
    /**
     * show dialog in case of edit and Add medicines information to the Data base
     */
    public final void showAlertDialog(@org.jetbrains.annotations.NotNull()
    com.medicineapp.roomdb.Medicine medicine) {
    }
    
    public MedicineActivity() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&\u00a8\u0006\b"}, d2 = {"Lcom/medicineapp/ui/medicinelist/MedicineActivity$OnItemClickListener;", "", "onItemClicked", "", "medicine", "Lcom/medicineapp/roomdb/Medicine;", "view", "Landroid/view/View;", "app_debug"})
    public static abstract interface OnItemClickListener {
        
        public abstract void onItemClicked(@org.jetbrains.annotations.NotNull()
        com.medicineapp.roomdb.Medicine medicine, @org.jetbrains.annotations.NotNull()
        android.view.View view);
    }
}
package com.medicineapp.ui.medicinelist;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\f\u0012\b\u0012\u00060\u0002R\u00020\u00000\u0001:\u0001\u001bB\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\b\u0010\u000f\u001a\u00020\u0010H\u0016J\u001c\u0010\u0011\u001a\u00020\u00122\n\u0010\u0013\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0014\u001a\u00020\u0010H\u0016J\u001c\u0010\u0015\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0010H\u0016J\u001b\u0010\u0019\u001a\u00020\u00122\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000bH\u0000\u00a2\u0006\u0002\b\u001aR\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e\u00a8\u0006\u001c"}, d2 = {"Lcom/medicineapp/ui/medicinelist/MedicineListAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/medicineapp/ui/medicinelist/MedicineListAdapter$MedicineViewHolder;", "context", "Landroid/content/Context;", "onItemClickListener", "Lcom/medicineapp/ui/medicinelist/MedicineActivity$OnItemClickListener;", "(Landroid/content/Context;Lcom/medicineapp/ui/medicinelist/MedicineActivity$OnItemClickListener;)V", "getContext", "()Landroid/content/Context;", "medicineList", "", "Lcom/medicineapp/roomdb/Medicine;", "getOnItemClickListener", "()Lcom/medicineapp/ui/medicinelist/MedicineActivity$OnItemClickListener;", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "setMedicineList", "setMedicineList$app_debug", "MedicineViewHolder", "app_debug"})
public final class MedicineListAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.medicineapp.ui.medicinelist.MedicineListAdapter.MedicineViewHolder> {
    private java.util.List<com.medicineapp.roomdb.Medicine> medicineList;
    @org.jetbrains.annotations.NotNull()
    private final android.content.Context context = null;
    @org.jetbrains.annotations.NotNull()
    private final com.medicineapp.ui.medicinelist.MedicineActivity.OnItemClickListener onItemClickListener = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.medicineapp.ui.medicinelist.MedicineListAdapter.MedicineViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.medicineapp.ui.medicinelist.MedicineListAdapter.MedicineViewHolder holder, int position) {
    }
    
    public final void setMedicineList$app_debug(@org.jetbrains.annotations.NotNull()
    java.util.List<com.medicineapp.roomdb.Medicine> medicineList) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getContext() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.medicineapp.ui.medicinelist.MedicineActivity.OnItemClickListener getOnItemClickListener() {
        return null;
    }
    
    public MedicineListAdapter(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.medicineapp.ui.medicinelist.MedicineActivity.OnItemClickListener onItemClickListener) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b\u00a8\u0006\t"}, d2 = {"Lcom/medicineapp/ui/medicinelist/MedicineListAdapter$MedicineViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Lcom/medicineapp/ui/medicinelist/MedicineListAdapter;Landroid/view/View;)V", "bind", "", "medicine", "Lcom/medicineapp/roomdb/Medicine;", "app_debug"})
    public final class MedicineViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.medicineapp.roomdb.Medicine medicine) {
        }
        
        public MedicineViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView) {
            super(null);
        }
    }
}
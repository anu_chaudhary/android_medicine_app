package com.medicineapp.ui.medicinelist;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u000e\u0010\u000b\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u0012\u0010\f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\u000e0\rR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"}, d2 = {"Lcom/medicineapp/ui/medicinelist/MedicineViewModel;", "Landroidx/lifecycle/AndroidViewModel;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "repository", "Lcom/medicineapp/ui/medicinelist/MedicineListRepository;", "cloneMedicineInfo", "", "medicine", "Lcom/medicineapp/roomdb/Medicine;", "deleteMedicine", "getMedicineList", "Landroidx/lifecycle/LiveData;", "", "app_debug"})
public final class MedicineViewModel extends androidx.lifecycle.AndroidViewModel {
    private com.medicineapp.ui.medicinelist.MedicineListRepository repository;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<com.medicineapp.roomdb.Medicine>> getMedicineList() {
        return null;
    }
    
    public final void deleteMedicine(@org.jetbrains.annotations.NotNull()
    com.medicineapp.roomdb.Medicine medicine) {
    }
    
    public final void cloneMedicineInfo(@org.jetbrains.annotations.NotNull()
    com.medicineapp.roomdb.Medicine medicine) {
    }
    
    public MedicineViewModel(@org.jetbrains.annotations.NotNull()
    android.app.Application application) {
        super(null);
    }
}
package com.medicineapp.utils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 1, 15}, bv = {1, 0, 3}, k = 2, d1 = {"\u0000\u001e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u000e\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003\u001a\u0012\u0010\u0004\u001a\u00020\u0005*\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b\u00a8\u0006\t"}, d2 = {"setBoldSpannable", "Landroid/text/SpannableStringBuilder;", "myText", "", "showToast", "", "", "context", "Landroid/content/Context;", "app_debug"})
public final class AppUtilsKt {
    
    public static final void showToast(@org.jetbrains.annotations.NotNull()
    java.lang.Object $this$showToast, @org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final android.text.SpannableStringBuilder setBoldSpannable(@org.jetbrains.annotations.NotNull()
    java.lang.String myText) {
        return null;
    }
}
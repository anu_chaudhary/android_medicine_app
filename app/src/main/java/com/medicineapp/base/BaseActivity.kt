package com.medicineapp.base

import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity;
import com.medicineapp.R
import kotlinx.android.synthetic.main.activity_base.*



abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_base)
        setLayout()
    }

    protected abstract fun getResourceId(): Int

    private fun setLayout() {
        if (getResourceId() != -1) {
            removeLayout()
            layoutInflater.inflate(getResourceId(), fl_base_container, true)
        }
    }


    private fun removeLayout() {
        if (fl_base_container!=null && fl_base_container.getChildCount() >= 1)
            fl_base_container.removeAllViews()
    }

}

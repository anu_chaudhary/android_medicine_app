package com.medicineapp.roomdb

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "medicine_table")
data class Medicine(
    var name : String,
    var description : String,
    var price : Double,
    var quantity : Long

)  {
    @PrimaryKey(autoGenerate = true)
    var id : Int =0


}
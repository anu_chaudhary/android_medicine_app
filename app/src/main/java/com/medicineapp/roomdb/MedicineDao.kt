package com.medicineapp.roomdb

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface MedicineDao {

    @Insert
    fun insertMedicine(medicine: Medicine)

    @Query("SELECT * FROM medicine_table")
    fun getMedicineList(): LiveData<List<Medicine>>

    @Query("SELECT * FROM medicine_table WHERE id == :medicineId")
    fun getMedicineInfo(medicineId : Int): Medicine

    @Update
    fun updateMedicine(medicine: Medicine)

    @Delete
    fun deleteMedicine(medicine: Medicine)
}
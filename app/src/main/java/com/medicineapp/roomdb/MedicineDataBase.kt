package com.medicineapp.roomdb

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase


@Database(entities = [Medicine::class],version = 1)
abstract class MedicineDataBase : RoomDatabase() {

    abstract fun medicineDao() : MedicineDao
    companion object {
        private var instance : MedicineDataBase ?= null
        fun getInstance(context: Context): MedicineDataBase? {
            if (instance == null) {
                synchronized(MedicineDataBase::class) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        MedicineDataBase::class.java, "medicine_database"
                    )
                        .allowMainThreadQueries()
                        .build()
                }
            }
            return instance
        }
    }

}
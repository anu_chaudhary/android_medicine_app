package com.medicineapp.ui

import android.os.AsyncTask
import com.medicineapp.roomdb.Medicine
import com.medicineapp.roomdb.MedicineDao

class InsertMedicineAsyncTask(medicineDao: MedicineDao) : AsyncTask<Medicine, Unit, Unit>() {
    val medicineDao = medicineDao

    override fun doInBackground(vararg p0: Medicine?) {
        medicineDao.insertMedicine(p0[0]!!)
    }
}
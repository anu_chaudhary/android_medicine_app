package com.medicineapp.ui.addmedicine

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.medicineapp.R
import com.medicineapp.roomdb.Medicine
import com.medicineapp.utils.showToast
import kotlinx.android.synthetic.main.dialog_add_medicine.*


class AddMedicineDialog : DialogFragment(), View.OnFocusChangeListener {
    private var medicineId: Int = -1
    private var isEdit: Boolean = false
    private var medicineInfo: Medicine? = null
    private val addMedicineViewModel: AddMedicineViewModel by lazy {
        ViewModelProviders.of(this).get(AddMedicineViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.DialogTheme);
        medicineId = arguments!!.getInt("medicine_id")
        if (medicineId != -1) {
            isEdit = true
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.dialog_add_medicine, null, false)
        if (isEdit)
            addMedicineViewModel.getMedicineInfo(medicineId)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        addMedicineViewModel.getLiveData().observe(this, Observer { addMedicineResponse ->
            when (addMedicineResponse.status) {
                Status.NAME_VALIDATION_ERROR -> til_medicine_name.error =
                    getString(com.medicineapp.R.string.valid_name)
                Status.DESC_VALIDATION_ERROR -> til_medicine_desc.error =
                    getString(com.medicineapp.R.string.valid_desc)
                Status.PRICE_VALIDATION_ERROR -> til_medicine_price.error =
                    getString(com.medicineapp.R.string.valid_price)
                Status.QTY_VALIDATION_ERROR -> til_medicine_qty.error =
                    getString(R.string.valid_qty)
                Status.NAME_VALIDATION_SUCCESS ->  til_medicine_name.error = ""
                Status.DESC_VALIDATION_SUCCESS ->  til_medicine_desc.error = ""
                Status.PRICE_VALIDATION_SUCCESS ->  til_medicine_price.error = ""
                Status.QTY_VALIDATION_SUCCESS ->  til_medicine_qty.error = ""
                Status.SUCCESS -> {
                    til_medicine_name.error = ""
                    til_medicine_desc.error = ""
                    til_medicine_price.error = ""
                    til_medicine_qty.error = ""
                    if (isEdit) {
                        medicineInfo!!.name = et_medicine_name.text.toString().trim()
                        medicineInfo!!.description = et_medicine_desc.text.toString().trim()
                        medicineInfo!!.price = et_medicine_price.text.toString().toDouble()
                        medicineInfo!!.quantity = et_medicine_qty.text.toString().toLong()
                        addMedicineViewModel.updateMedicine(medicineInfo!!)
                        getString(R.string.edit_medicine_success).showToast(context!!)

                    } else {
                        addMedicineViewModel.addMedicine(
                            et_medicine_name.text.toString().trim(),
                            et_medicine_desc.text.toString().trim(),
                            et_medicine_price.text.toString().trim(),
                            et_medicine_qty.text.toString().trim()
                        )
                        getString(R.string.add_medicine_success).showToast(context!!)
                    }
                    dismiss()
                }
                Status.GET_MEDICINE_INFO_SUCCESS -> {
                    medicineInfo = addMedicineResponse.medicine
                    et_medicine_name.setText(medicineInfo?.component1().toString())
                    et_medicine_name.setSelection(et_medicine_name.text!!.length)
                    et_medicine_desc.setText(medicineInfo?.component2().toString())
                    et_medicine_price.setText(medicineInfo?.component3().toString())
                    et_medicine_qty.setText(medicineInfo?.component4().toString())
                    btn_add_medicine.text = getString(R.string.edit_medicine)
                }
            }
        })

        btn_add_medicine.setOnClickListener {
            addMedicineViewModel.validateForm(
                et_medicine_name.text.toString().trim(),
                et_medicine_desc.text.toString().trim(),
                et_medicine_price.text.toString().trim(),
                et_medicine_qty.text.toString().trim()
            )
        }
        btn_cancel.setOnClickListener { dismiss() }

        et_medicine_name.onFocusChangeListener = this
        et_medicine_desc.onFocusChangeListener = this
        et_medicine_price.onFocusChangeListener = this
        et_medicine_qty.onFocusChangeListener = this
    }

    /**
     * get new instance of medicine dialog with some initial arguments
     */
    companion object {
        fun newInstance(medicineId: Int): AddMedicineDialog {
            val fragment = AddMedicineDialog()
            val arg = Bundle()
            arg.putInt("medicine_id", medicineId)
            fragment.arguments = arg
            return fragment
        }
    }

    /**
     * check fields validations on focus change
     */
    override fun onFocusChange(p0: View?, p1: Boolean) {
        if (!p1) {
            when (p0!!.id) {
                R.id.et_medicine_name -> addMedicineViewModel.validateFields(
                    et_medicine_name.text.toString().trim(),
                    Status.NAME_VALIDATION_SUCCESS,
                    Status.NAME_VALIDATION_ERROR
                )
                R.id.et_medicine_desc -> addMedicineViewModel.validateFields(
                    et_medicine_desc.text.toString().trim(),
                    Status.DESC_VALIDATION_SUCCESS,
                    Status.DESC_VALIDATION_ERROR
                )
                R.id.et_medicine_price -> addMedicineViewModel.validateFields(
                    et_medicine_price.text.toString().trim(),
                    Status.PRICE_VALIDATION_SUCCESS,
                    Status.PRICE_VALIDATION_ERROR
                )
                R.id.et_medicine_qty -> addMedicineViewModel.validateFields(
                    et_medicine_qty.text.toString().trim(),
                    Status.QTY_VALIDATION_SUCCESS,
                    Status.QTY_VALIDATION_ERROR
                )
            }
        }
    }


}
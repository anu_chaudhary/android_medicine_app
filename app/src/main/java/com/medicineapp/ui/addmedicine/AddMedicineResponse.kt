package com.medicineapp.ui.addmedicine

import com.medicineapp.roomdb.Medicine


data class AddMedicineResponse(val status: Status, val medicine: Medicine?, val error: Exception?)
enum class Status {
    NAME_VALIDATION_ERROR,
    DESC_VALIDATION_ERROR,
    PRICE_VALIDATION_ERROR,
    QTY_VALIDATION_ERROR,
    NAME_VALIDATION_SUCCESS,
    DESC_VALIDATION_SUCCESS,
    PRICE_VALIDATION_SUCCESS,
    QTY_VALIDATION_SUCCESS,
    SUCCESS,
    GET_MEDICINE_INFO_SUCCESS,
}



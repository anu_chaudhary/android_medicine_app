package com.medicineapp.ui.addmedicine

import android.app.Application
import android.text.TextUtils
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.medicineapp.roomdb.Medicine

class AddMedicineViewModel(application: Application) : AndroidViewModel(application) {

    private var repository : MedicineRepository = MedicineRepository(application)
    private var mutableAddMedicineData : MutableLiveData<AddMedicineResponse> = MutableLiveData()

    /**
     * return liva data to the view to be observe applied changes
     */
    fun getLiveData():MutableLiveData<AddMedicineResponse>{
        return mutableAddMedicineData
    }

    /**
     *  fetch medicine details from data base in case of any CURD operations
     */
    fun getMedicineInfo(medicineId : Int)
    {
        mutableAddMedicineData.value = AddMedicineResponse(Status.GET_MEDICINE_INFO_SUCCESS,repository.getMedicine(medicineId),null)
    }


    fun validateForm(name : String,desc : String,price : String,qty: String)
    {
        if(TextUtils.isEmpty(name))
        {
            mutableAddMedicineData.value = AddMedicineResponse(Status.NAME_VALIDATION_ERROR,null,null)
        }else if(TextUtils.isEmpty(desc))
        {
            mutableAddMedicineData.value = AddMedicineResponse(Status.DESC_VALIDATION_ERROR,null,null)
        }else if(TextUtils.isEmpty(price))
        {
            mutableAddMedicineData.value = AddMedicineResponse(Status.PRICE_VALIDATION_ERROR,null,null)
        }else if(TextUtils.isEmpty(qty))
        {
            mutableAddMedicineData.value = AddMedicineResponse(Status.QTY_VALIDATION_ERROR,null,null)
        }else{
            mutableAddMedicineData.value = AddMedicineResponse(Status.SUCCESS,null,null)
        }

    }

    fun addMedicine(name : String,desc : String,price : String,qty: String){
        var medicine = Medicine(name,desc,price.toDouble(),qty.toLong())
        repository.insertMedicine(medicine)
    }

    fun updateMedicine( medicine: Medicine){
        repository.updateMedicine(medicine)
    }

   fun validateFields(value: String,success : Status, error : Status)
   {
       if(TextUtils.isEmpty(value))
       {
           mutableAddMedicineData.value = AddMedicineResponse(error,null,null)
       }else{
           mutableAddMedicineData.value = AddMedicineResponse(success,null,null)
       }
   }


}
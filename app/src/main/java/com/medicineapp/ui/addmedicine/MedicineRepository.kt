package com.medicineapp.ui.addmedicine

import android.app.Application
import android.os.AsyncTask
import com.medicineapp.roomdb.Medicine
import com.medicineapp.roomdb.MedicineDao
import com.medicineapp.roomdb.MedicineDataBase
import com.medicineapp.ui.InsertMedicineAsyncTask

class MedicineRepository(application: Application) {
    private  var medicineDao : MedicineDao

    init {
        val medicineDataBase : MedicineDataBase = MedicineDataBase.getInstance(application)!!
        medicineDao = medicineDataBase.medicineDao()
    }

    fun insertMedicine(medicine: Medicine) {
         InsertMedicineAsyncTask(medicineDao).execute(medicine)
    }

    fun getMedicine(medicineId : Int) : Medicine {
        return medicineDao.getMedicineInfo(medicineId)
    }

    fun updateMedicine(medicine: Medicine){
        UpdateMedicineAsyncTask(medicineDao).execute(medicine)
    }


    private class UpdateMedicineAsyncTask(medicineDao: MedicineDao) : AsyncTask<Medicine, Unit, Unit>() {
        val medicineDao = medicineDao

        override fun doInBackground(vararg p0: Medicine?) {
            medicineDao.updateMedicine(p0[0]!!)
        }
    }

}
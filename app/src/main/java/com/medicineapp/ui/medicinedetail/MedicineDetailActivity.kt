package com.medicineapp.ui.medicinedetail

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.medicineapp.R
import com.medicineapp.base.BaseActivity
import com.medicineapp.ui.addmedicine.Status
import com.medicineapp.utils.setBoldSpannable
import kotlinx.android.synthetic.main.activity_medicine_detail.*
import kotlinx.android.synthetic.main.toolbar.*

class MedicineDetailActivity : BaseActivity() {
    private var medicineId: Int = -1
    private val medicineDetailViewModel : MedicineDetailViewModel by lazy {
        ViewModelProviders.of(this).get(MedicineDetailViewModel::class.java)
    }


    override fun getResourceId(): Int {
        return R.layout.activity_medicine_detail
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        medicineId = intent!!.getIntExtra("medicine_id",-1)
        medicineDetailViewModel.getMedicineInfo(medicineId)

        iv_close.visibility = View.VISIBLE
        iv_close.setOnClickListener{
            finish()
        }

        tv_title.text = getString(R.string.medicine_detail)
        medicineDetailViewModel.getLiveData().observe(this, Observer { addMedicineResponse ->
            when (addMedicineResponse.status) {
                Status.GET_MEDICINE_INFO_SUCCESS -> {
                    val medicineInfo = addMedicineResponse.medicine
                    tv_name.text = medicineInfo?.component1().toString()
                    tv_desc.text = medicineInfo?.component2().toString()
                    tv_price.text = setBoldSpannable(getString(R.string.price_label)).append(getString(R.string.rupee_symbol)+medicineInfo?.component3().toString())
                    tv_qty.text = setBoldSpannable(getString(R.string.qty_label)).append(medicineInfo?.component4().toString())
                }
            }
        })
    }
}

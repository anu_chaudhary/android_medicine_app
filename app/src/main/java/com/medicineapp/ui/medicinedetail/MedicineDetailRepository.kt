package com.medicineapp.ui.medicinedetail

import android.app.Application
import com.medicineapp.roomdb.Medicine
import com.medicineapp.roomdb.MedicineDao
import com.medicineapp.roomdb.MedicineDataBase

class MedicineDetailRepository(application: Application) {
    private var medicineDao: MedicineDao

    init {
        val medicineDataBase: MedicineDataBase = MedicineDataBase.getInstance(application)!!
        medicineDao = medicineDataBase.medicineDao()
    }

    fun getMedicineDetail(medicineId : Int) : Medicine {
        return medicineDao.getMedicineInfo(medicineId)
    }

}
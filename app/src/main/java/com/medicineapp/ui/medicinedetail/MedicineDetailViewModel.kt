package com.medicineapp.ui.medicinedetail

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.medicineapp.ui.addmedicine.AddMedicineResponse
import com.medicineapp.ui.addmedicine.Status

class MedicineDetailViewModel(application: Application) : AndroidViewModel(application) {

    private var repository : MedicineDetailRepository = MedicineDetailRepository(application)
    private var medicineDetailData : MutableLiveData<AddMedicineResponse> = MutableLiveData()


    fun getLiveData():MutableLiveData<AddMedicineResponse>{
        return medicineDetailData
    }

    fun getMedicineInfo(medicineId : Int)
    {
        medicineDetailData.value = AddMedicineResponse(Status.GET_MEDICINE_INFO_SUCCESS,repository.getMedicineDetail(medicineId),null)
    }

}
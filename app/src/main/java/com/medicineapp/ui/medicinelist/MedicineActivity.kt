package com.medicineapp.ui.medicinelist

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.medicineapp.R
import com.medicineapp.base.BaseActivity
import com.medicineapp.roomdb.Medicine
import com.medicineapp.ui.addmedicine.AddMedicineDialog
import com.medicineapp.ui.medicinedetail.MedicineDetailActivity
import com.medicineapp.utils.showToast
import kotlinx.android.synthetic.main.activity_medicine.*


class MedicineActivity : BaseActivity() {

    private val medicineViewModel : MedicineViewModel by lazy {
        ViewModelProviders.of(this).get(MedicineViewModel::class.java)
    }

    override fun getResourceId(): Int {
       return R.layout.activity_medicine
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        fl_add_medicine.setOnClickListener{
            openAddMedicineDialog(-1)
        }

        val adapter = MedicineListAdapter(this,object : OnItemClickListener {
            override fun onItemClicked(medicine: Medicine, view: View) = when(view.id) {
                R.id.btn_clone_medicine -> {
                    medicine.id = 0
                    medicineViewModel.cloneMedicineInfo(medicine)
                    getString(R.string.clone_medicine_success).showToast(this@MedicineActivity)
                }
                R.id.btn_delete_medicine -> showAlertDialog(medicine)
                R.id.btn_edit_medicine ->  openAddMedicineDialog(medicine.id)
                else -> {
                    openMedicineDetailPage(medicine.id)
                }
            }

        })
        rv_medicine_list?.layoutManager = LinearLayoutManager(this)
        rv_medicine_list?. adapter = adapter

        medicineViewModel.getMedicineList().observe(this, object : Observer<List<Medicine>> {
            override fun onChanged(medicinelist: List<Medicine>?) {
                if(medicinelist!!.isEmpty())
                {
                    tv_no_data.visibility = View.VISIBLE
                    rv_medicine_list.visibility = View.GONE
                }else {
                    tv_no_data.visibility = View.GONE
                    rv_medicine_list.visibility = View.VISIBLE
                    adapter.setMedicineList(medicinelist!!)
                }
            }
        })
    }

    /**
     * open detail page as per medicine selection
     */
    fun openMedicineDetailPage(medicineId: Int){
        val intent = Intent(this, MedicineDetailActivity::class.java)
        intent.putExtra("medicine_id",medicineId)
        startActivity(intent)
    }

  private  fun openAddMedicineDialog(medicineId : Int){
        val fragmentTransaction : FragmentTransaction = supportFragmentManager.beginTransaction()
        var fragment : Fragment? = supportFragmentManager.findFragmentByTag("dialog")
        fragment?.let { fragmentTransaction.remove(it) }
        fragmentTransaction.addToBackStack(null)

        val addMedicineDialog  = AddMedicineDialog.newInstance(medicineId)
        addMedicineDialog.show(fragmentTransaction,"dialog")
    }

    interface OnItemClickListener {
        fun onItemClicked(medicine: Medicine, view: View)
    }

    /**
     * show dialog in case of edit and Add medicines information to the Data base
     */
    fun showAlertDialog(medicine: Medicine){
        val dialogBuilder = AlertDialog.Builder(this)
        dialogBuilder.setMessage(getString(R.string.delete_alert_msg))
            .setCancelable(false)
            .setPositiveButton(getString(R.string.proceed), DialogInterface.OnClickListener {
                    dialog, id ->
                medicineViewModel.deleteMedicine(medicine)
                getString(R.string.delete_medicine_success).showToast(this@MedicineActivity)
            })
            .setNegativeButton(getString(R.string.cancel), DialogInterface.OnClickListener {
                    dialog, id -> dialog.cancel()
            })
        val alert = dialogBuilder.create()
        alert.setTitle(getString(R.string.delete_medicine))
        alert.show()
    }

}

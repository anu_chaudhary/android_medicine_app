package com.medicineapp.ui.medicinelist

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.medicineapp.R
import com.medicineapp.roomdb.Medicine
import com.medicineapp.utils.setBoldSpannable
import kotlinx.android.synthetic.main.row_medicine_list.view.*

class MedicineListAdapter (val context: Context,val onItemClickListener: MedicineActivity.OnItemClickListener) : RecyclerView.Adapter<MedicineListAdapter.MedicineViewHolder>() {
    private var medicineList: List<Medicine> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MedicineViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.row_medicine_list, parent, false)
        return MedicineViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return medicineList.size
    }

    override fun onBindViewHolder(holder: MedicineViewHolder, position: Int) {
         holder.bind(medicineList.get(position))
    }

    inner class MedicineViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun bind(medicine: Medicine) {
            itemView.tv_name.text = medicine.name
            itemView.tv_desc.text = medicine.description
            itemView.tv_price.text = setBoldSpannable(context.getString(R.string.price_label)).append(context.getString(R.string.rupee_symbol)+medicine.price.toString())
            itemView.tv_qty.text = setBoldSpannable(context.getString(R.string.qty_label)).append(medicine.quantity.toString())

            itemView.btn_clone_medicine.setOnClickListener {
                onItemClickListener.onItemClicked(medicine,it)
            }
            itemView.btn_delete_medicine.setOnClickListener {
                onItemClickListener.onItemClicked(medicine,it)
            }
            itemView.btn_edit_medicine.setOnClickListener {
                onItemClickListener.onItemClicked(medicine,it)
            }
            itemView.setOnClickListener {
                onItemClickListener.onItemClicked(medicine,it)
            }
        }
    }


    internal fun setMedicineList(medicineList : List<Medicine>)
    {
         this.medicineList = medicineList
          notifyDataSetChanged()

    }
}
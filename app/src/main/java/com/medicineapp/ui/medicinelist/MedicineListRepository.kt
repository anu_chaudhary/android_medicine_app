package com.medicineapp.ui.medicinelist

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.medicineapp.roomdb.Medicine
import com.medicineapp.roomdb.MedicineDao
import com.medicineapp.roomdb.MedicineDataBase
import com.medicineapp.ui.InsertMedicineAsyncTask

class MedicineListRepository(application: Application) {
    private var medicineDao: MedicineDao
    private var medincineList: LiveData<List<Medicine>>

    init {
        val medicineDataBase: MedicineDataBase = MedicineDataBase.getInstance(application)!!
        medicineDao = medicineDataBase.medicineDao()
        medincineList = medicineDao.getMedicineList()
    }

    fun getAllMedicineList(): LiveData<List<Medicine>> {
          return medincineList
    }

    fun deleteMedicine(medicine: Medicine) {
        DeleteMedicineAsyncTask(medicineDao).execute(medicine)
    }

    fun cloneMedicine(medicine: Medicine) {
        InsertMedicineAsyncTask(medicineDao).execute(medicine)
    }

    private class DeleteMedicineAsyncTask(medicineDao: MedicineDao) : AsyncTask<Medicine, Unit, Unit>() {
        val medicineDao = medicineDao

        override fun doInBackground(vararg p0: Medicine?) {
            medicineDao.deleteMedicine(p0[0]!!)
        }
    }

}
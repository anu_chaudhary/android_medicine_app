package com.medicineapp.ui.medicinelist

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.medicineapp.roomdb.Medicine

class MedicineViewModel(application: Application) : AndroidViewModel(application){
    private var repository : MedicineListRepository = MedicineListRepository(application)


    fun getMedicineList() : LiveData<List<Medicine>> {
       return repository.getAllMedicineList()
    }

    fun deleteMedicine(medicine: Medicine){
        repository.deleteMedicine(medicine)
    }

    fun cloneMedicineInfo(medicine: Medicine){
        repository.cloneMedicine(medicine)
    }
}
package com.medicineapp.utils

import android.content.Context
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan
import android.widget.Toast


fun Any.showToast(context : Context){
    Toast.makeText(context,this.toString(),Toast.LENGTH_SHORT).show()

}

fun setBoldSpannable(myText: String): SpannableStringBuilder {
    val spannableContent = SpannableStringBuilder(myText)
    spannableContent.setSpan(StyleSpan(Typeface.BOLD), 0,  myText.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    return spannableContent
}

